<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="danhsach.css">
    <title>DSSV</title>
    
</head>


<body>

<div class="header">
    <div class="header1">
        <div class="item">
            <div class="item_left">Khoa</div>
            <div class="item_right">
                <select class="drop" name ='select_khoa'>
                    <?php
                        $phankhoa = array(
                            "MTA" => "Khoa học máy tính",
                            "KDL" => "Khoa học dữ liệu"

                        );
                        echo "<option value='' class='chon1'></option>"; //du liệu để trống

                        foreach($phankhoa as $class => $val) { // dùng foreach để lấy dữ liệu từ mảng
                            echo "<option value='$val' class='chon1 ' >$val</option>";     
                        }   
                    ?>   
                </select>
            </div>        
        </div>

        <div class="item">
            <div class="item_left">Từ khóa</div>
            <div class="item_right">
                <input type="text" class="keyword">                
            </div>
        </div>

        <button class="search">Tìm kiếm</button>

    </div>

    <div class="item left show_sv">Số sinh viên tìm thấy:</div>
    <div class="header2">
        
        <div class="item_left2">
            <div class="item_left_2">
                <div class="item_left_con1">No</div>
                <div class="item_left_con">Tên sinh viên</div>
                <div class="item_left_con">Khoa</div>
            </div>

            <div class="item_left_2">
                <div class="item_left_con1">1</div>
                <div class="item_left_con">Nguyễn Văn A</div>
                <div class="item_left_con">Khoa học máy tính</div>
            </div>
            <div class="item_left_2">
                <div class="item_left_con1">2</div>
                <div class="item_left_con">Nguyễn Văn A</div>
                <div class="item_left_con">Khoa học máy tính</div>
            </div>
            <div class="item_left_2">
                <div class="item_left_con1">3</div>
                <div class="item_left_con">Nguyễn Văn A</div>
                <div class="item_left_con">Khoa học vật liệu</div>
            </div>
            <div class="item_left_2">
                <div class="item_left_con1">4</div>
                <div class="item_left_con">Nguyễn Văn A</div>
                <div class="item_left_con">Khoa học vật liệu</div>
            </div>
            
        </div>

        <div class="item_right2">
            
                <a href="login.php" class="add">Thêm</a>
            
            <div class="action">Action</div>
            <div class="item_right_con">
                <div class="delete">Xóa</div>
                <div class="delete">Sửa</div>
            </div>
            <div class="item_right_con">
                <div class="delete">Xóa</div>
                <div class="delete">Sửa</div>
            </div>
            <div class="item_right_con">
                <div class="delete">Xóa</div>
                <div class="delete">Sửa</div>
            </div>
            <div class="item_right_con">
                <div class="delete">Xóa</div>
                <div class="delete">Sửa</div>
            </div>
        </div>

        
    </div>

</div>
    
</body>
</html>